# Spevnik mladeznickych piesni
The aim of this project is to prepare songbook texts to be displayed on screens at church.

## Source and its preparation for a python script 
The source of the texts was get from Pavol Kalata kalata@saleziani.sk based on their website http://mladeznickyspevnik.sk/. He shared a word document which can be found in the repository. The texts were semi-automatically prepared for the python script, especially wrong formatting was corrected.
Song numbers and names were extracted to a new *song_names.txt* file. Texts were corrected in Notepad++ within another *texts_raw.txt* file.

## Python script

### Used packages
The python *re* package was used for working with regex expressions. The second package to be imported is *json* for creating the json output.
### Script

#### Song names
Song names are extracted from the *song_names.txt* file. The file is splitted by enter into a list of songs. Each line is splitted by tab into a number and name. 
#### Songs with strophes
At the beginning of a new strophe, you can find the following characters: numbers, "Br: ","Br1: ", "Br2: ", "R: ", "R1: ", "R2: ", "Z: ". The text is splitted into strophes based on regex expression including all these cases. Then, each strophe is divided into lines.
#### Songs without strophes
The song is splitted only by enters into a single strophe.
#### Output json
The result of the cycle for each song is exported into the output file *OutputSMP.json*.