import re # package for regex expressions
import json # import package for json format

# get song names from song_names.txt
with open('song_names.txt', encoding='utf8') as fp:
    # each song name is on a separate line
    song_names_list = fp.read().split('\n')
    names_dictionary = {}
    for song_name in song_names_list:
        # take the first element before tab without a dot as number
        number = song_name.split('\t')[0].strip('.')
        # take text after tab as name
        name = song_name.split('\t')[1]
        song_dict = {number:name} # create a dictionary couple
        names_dictionary.update(song_dict) # add it to the names dictionary

# prepare json for the future output file
output_json = {"songbook": "Spevnik mladeznickych piesni", "songs": []}

# get texts from the source txt
with open('texts_raw.txt', encoding='utf8') as fp:
    songs_list = re.split(r"\d+."+"identifier\n",fp.read()) # split texts into list of song strings
    del songs_list[0] # delete the empty first element 

    for index, song in enumerate(songs_list):
        song_number = index + 1 # get song number using index
        song_name = names_dictionary.get(str(song_number)) # get name based on the index
        # prepare song json 
        song_json = {"id":str(song_number), "name":song_name, "verses":[]}

        # format with strophes 
        if(re.search(r"\d+.", song)): # for song with at least one numbered strophe
            verses_list = re.split(r"\d+. |R: |R:|Z: |R1: |R2: |Br: |Br1: |Br2: ", song) # split strophes by any of the following strings
            del verses_list[0] # delete the empty string
            for verse in verses_list: # go through each strophe
                verse = verse.strip("\n") # remove extra enters
                verse_output = []
                lines = verse.split("\n") # split lines
                for line in lines:
                    line = line.strip()
                    verse_output.append(line)
                song_json["verses"].append(verse_output) 

        # format without strophes
        else:
            verse_output = []
            song = song.strip("\n\n").strip("\n") # remove extra enters
            lines = song.split("\n") # split by enter
            for line in lines:
                line = line.strip() # remove extra spaces
                verse_output.append(line)
            song_json["verses"].append(verse_output)

        # add song to the overall json
        output_json["songs"].append(song_json)

# generate output        
with open('OutputSMP.json', 'w', encoding='utf8') as fp:
    json.dump(output_json, fp, indent = 2, ensure_ascii=False)